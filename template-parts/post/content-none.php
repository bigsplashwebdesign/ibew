<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-header"><h1 class="entry-title">Nothing Found</h1></div><!-- .entry-header -->

	<div class="entry-content">
		<p>This page appears to be missing or never existed.</p>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->
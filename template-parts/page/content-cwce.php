<!-- banner start -->
<section class="cwce-banner sp-banner">
  <div class="container"> 
    <div class="row">
      <div class="col-12 col-sm-12">
        <div class="banner-content">
          <div class="logo">
            <a href="<?php echo home_url(); ?>">
           <?php $header_logo = get_field('upload_website_logo', 'option'); ?>
          <?php if ( $header_logo ) : ?>
          <img src="<?php echo $header_logo['url']; ?>" alt="<?php echo $header_logo['alt']; ?>" />
          <?php endif; ?>
          </a>
          </div>
          <div class="title">
            <h1><?php echo get_the_title(); ?></h1>
          </div>
        </div>
      </div>        
    </div>
  </div>
</section>
<!-- banner end -->

<!-- journeyman-industry -->
<section class="journeyman-industry">
  <div class="container">
    <div class="row g-0">
      <div class="col-12 col-sm-12 col-md-10 col-lg-8 col-xl-6" >
        <div class="col-left">
          <h2><?php the_field('cw_ce_title'); ?></h2>
          <?php the_field('cw_ce_content'); ?>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- journeyman-industry-end -->
<!-- pay-dues -->
<section class="pay-dues-sp">
<!--pay-dues banner start -->
<section class="pay-dues-banner">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12">
        <div class="banner-content">
          <div class="logo">
            <a href="<?php echo home_url(); ?>">
           <?php $header_logo = get_field('upload_website_logo', 'option'); ?>
          <?php if ( $header_logo ) : ?>
          <img src="<?php echo $header_logo['url']; ?>" alt="<?php echo $header_logo['alt']; ?>" />
          <?php endif; ?>
          </a>
          </div>
          <div class="title">
            <h1><?php echo get_the_title(); ?></h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- pay-dues banner end -->

<!-- pay-online -->
<section class="pay-online">
  <div class="container">
    <div class="row justify-content-center text-center">
      <div class="col-12 col-lg-6">
        <div class="content">
          <h2>
            <?php the_field('pay_online_title'); ?>
          </h2>
          <span>
           <?php the_field('pay_online_sub_title'); ?>
          </span>
          <div class="link">
            <a href="<?php the_field('apple_icon_link'); ?>" target="_blank">
             
              <?php $apple_icon = get_field('upload_apple_icon');
                if( !empty($apple_icon) ): ?>
                 <img src="<?php echo $apple_icon['url']; ?>" alt="<?php echo $apple_icon['alt']; ?>" />
                 <?php endif; ?>
            </a>
            <a href="<?php the_field('google_play_icon_link'); ?>" target="_blank">
                <?php $google_icon = get_field('upload_google_play_icon');
                if( !empty($google_icon) ): ?>
                 <img src="<?php echo $google_icon['url']; ?>" alt="<?php echo $google_icon['alt']; ?>" />
                 <?php endif; ?>
            </a>
          </div>
          <a class="sm-btn" href="<?php the_field('pay_your_dues_online_button_link'); ?>">
            <?php the_field('pay_your_dues_online_button_label'); ?>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- pay-online -->

<!-- notice on dues -->
<section class="notice-dues">
  <div class="container p-xxl-0">
    <div class="row g-0">
      <div class="col-12 col-sm-10 col-md-8 col-lg-6">
        <div class="content">
          <h2><?php the_field('notice_dues_title'); ?></h2>
          <?php the_field('notice_dues_content'); ?>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- otice on dues -->
<!-- pay-dues-sp -->
</section>

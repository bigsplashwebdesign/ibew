<!--contact-page banner start -->
<section class="contact-page-banner">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12">
        <div class="banner-content">
        <div class="logo">
          <a href="<?php echo home_url(); ?>">
           <?php $header_logo = get_field('upload_website_logo', 'option'); ?>
          <?php if ( $header_logo ) : ?>
          <img src="<?php echo $header_logo['url']; ?>" alt="<?php echo $header_logo['alt']; ?>" />
          <?php endif; ?>
          </a>
        </div>
        <div class="title">
          <h1><?php echo get_the_title(); ?></h1>
        </div>
      </div>
      </div>
    </div>
  </div>
</section>
<!-- contact-page banner end -->

<!-- contact us -->
<section class="contact-us">
  <div class="container">
    <div class="row g-0 d-flex justify-content-between">
      <div class="col-12 col-sm-12 col-md-5">
        <div class="col-left">
          <h2><?php the_field('contact_title'); ?></h2>
          <p><?php the_field('contact_description'); ?></p>
          <div class="contact-us-detail">
            <ul>
              <li class="location">
                <span><?php the_field('location_title', 'option'); ?></span><a target="" href="<?php the_field('address_link', 'option'); ?>"> <?php the_field('address', 'option'); ?></a>
              </li>
              <li class="">
                <span><?php the_field('phone_title', 'option'); ?></span><a href="tel:713-869-8900"><?php the_field('header_phone_number', 'option'); ?></a>
              </li>
              <li class="">
                <span><?php the_field('fax_title', 'option'); ?></span><a href="tel:713-868-6342"><?php the_field('fax_number', 'option'); ?></a>
              </li>
              <li class="">
                <span><?php the_field('job_line', 'option'); ?></span><a href="tel:713-869-1311 "><?php the_field('job_line_number', 'option'); ?></a>
              </li>
              <li class="">
                <span><?php the_field('toll_free_title', 'option'); ?></span><a href="tel:888-716-JOBS"><?php the_field('toll_free_number', 'option'); ?></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-12 col-md-6 mt-5 mt-md-0">
        <div class="col-right">
<!--         <form class="row g-3">
          <div class="col-md-6">
            <label for="inputEmail4" class="form-label ">First Name</label>
            <input type="text" class="form-control" id="inputEmail4" placeholder="First Name">
          </div>
          <div class="col-md-6">
            <label for="inputEmail4" class="form-label">Last Name</label>
            <input type="text" class="form-control" id="inputEmail4" placeholder="Last Name">
          </div>
          <div class="col-md-6">
            <label for="inputPassword4" class="form-label">Email</label>
            <input type="email" class="form-control" id="inputPassword4" placeholder="Email">
          </div>
          <div class="col-md-6">
            <label for="inputAddress" class="form-label">Phone number</label>
            <input type="number" class="form-control" id="inputAddress" placeholder="Number">
          </div>
          <div class="text-area-contactus d-flex flex-column">
          <label for="" class="form-label">Message</label>
          <textarea name="" id="" rows="3" placeholder="Additional information"></textarea>
          </div>
          <button class="sm-btn" type="submit">submit</button>
          </div>
        </form> -->
		  	<?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>
      </div>
      </div>
    </div>
  </div>
</section>
<!-- contact us end -->
<!-- become a member sub page -->
<!-- banner -->
<div class="become-member-banner sp-banner">
  <div class="container"> 
    <div class="row">
      <div class="col-12 col-sm-12">
        <div class="banner-content">
          <div class="logo">
            <a href="<?php echo home_url(); ?>">
           <?php $header_logo = get_field('upload_website_logo', 'option'); ?>
          <?php if ( $header_logo ) : ?>
          <img src="<?php echo $header_logo['url']; ?>" alt="<?php echo $header_logo['alt']; ?>" />
          <?php endif; ?>
          </a>
          </div>
          <div class="title">
            <h1><?php echo get_the_title(); ?></h1>
          </div>
        </div>
      </div>        
    </div>
  </div>
</div>
<!-- join-ibew -->
<div class="become-member-join">
  <div class="container-fluid p-xl-0">
    <div class="row ">
      <div class="col-12 col-lg-6 col-md-6 col-sm-12 d-flex justify-content-start justify-content-md-end">
        <div class="col-left">
            <h2><?php the_field('become_a_member_main_title'); ?></h2>
              <?php the_field('member_join_content'); ?>
        </div>
      </div>
      <div class="col-12 col-sm-12 col-md-6 col-lg-6">
        <div class="col-right">
          <?php the_field('member_join_right_content'); ?>
        </div>
      </div>
    </div>
  </div>

</div>
<!-- best of best btf2 -->
<div class="become-member-best">
  <div class="container-fluid p-lg-0">
    <div class="row g-0">
      <div class="col-12 col-md-5 col-sm-12 d-flex justify-content-md-end justify-content-start">
       <div class="col-left">
         <h3>
           <?php the_field('best_main_title'); ?>
         </h3>
         <p class="m-0">
         <?php the_field('best_content'); ?>         
         </p>
       </div>
      </div>
      <div class="col-12 col-sm-12 col-md-7 d-flex">
        <div class="col-right">
          <iframe class="iframe-vid" src="<?php the_field('best_right_video'); ?>" width="100%" height="465" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="brotherhood-benefit">
  <div class="container">
      <h2><?php the_field('benefits_title'); ?> </h2>
    <div class="row ">
      <div class="col-12 col-sm-12 col-lg-7 col-xl-6">
        <div class="col-left">  
          <?php if (have_rows('benefits_list')) { ?>
          <ul>
             <?php while (have_rows('benefits_list')) { the_row(); ?>
            <li><?php the_sub_field('label'); ?></li>
              <?php } ?>
          </ul>
          <?php } ?>
        </div>
      </div>
      <div class="col-12 col-sm-12 col-lg-5 col-xl-6">
        <div class="col-right">
          <h4><?php the_field('requirements_to_join_title'); ?> </h4>
           <?php if (have_rows('requirements_to_join_list')) { ?>
          <ul>
              <?php while (have_rows('requirements_to_join_list')) { the_row(); ?>
            <li><?php the_sub_field('label'); ?></li>
             <?php } ?>   
          </ul>
           <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- become a member sub page -->

<!-- contact us -->
<section class="contact-us">
  <div class="container pt-xl-3 pb-xl-3">
    <div class="row g-0 d-flex justify-content-between">
      <div class="col-12 col-sm-12 col-md-5">
        <div class="col-left">
          <h2 class="mb-4"><?php the_field('how_to_join_title'); ?></h2>
          <p><?php the_field('how_to_join_content'); ?>
            </p>
        </div>
      </div>
      <div class="col-12 col-sm-12 col-md-6 mt-5 mt-md-0">
        <div class="col-right">
        <form class="row g-3">
          <div class="col-md-6">
            <label for="inputEmail4" class="form-label ">First Name</label>
            <input type="text" class="form-control" id="inputEmail4" placeholder="First Name">
          </div>
          <div class="col-md-6">
            <label for="inputEmail4" class="form-label">Last Name</label>
            <input type="text" class="form-control" id="inputEmail4" placeholder="Last Name">
          </div>
          <div class="col-md-6">
            <label for="inputPassword4" class="form-label">Email</label>
            <input type="email" class="form-control" id="inputPassword4" placeholder="Email">
          </div>
          <div class="col-md-6">
            <label for="inputAddress" class="form-label">Phone number</label>
            <input type="number" class="form-control" id="inputAddress" placeholder="Number">
          </div>
          <div class="text-area-contactus d-flex flex-column">
          <label for="" class="form-label">Message</label>
          <textarea name="" id="" rows="3" placeholder="Additional information"></textarea>
          </div>
          <button class="sm-btn" type="submit">submit</button>
          </div>
        </form>
      </div>
      </div>
    </div>
  </div>
</section>

<!-- become an apprentice sub page -->
<!-- banner -->
<div class="become-apprentice-banner sp-banner">
  <div class="container"> 
    <div class="row">
      <div class="col-12 col-sm-12">
        <div class="banner-content">
          <div class="logo">
           <a href="<?php echo home_url(); ?>">
               <?php $header_logo = get_field('upload_website_logo', 'option'); ?>
              <?php if ( $header_logo ) : ?>
              <img src="<?php echo $header_logo['url']; ?>" alt="<?php echo $header_logo['alt']; ?>" />
              <?php endif; ?>
          </a>
          </div>
          <div class="title">
            <h1><?php echo get_the_title(); ?></h1>
          </div>
        </div>
      </div>        
    </div>
  </div>
</div>
<!-- joinibew -->
<div class="Jatc-training">
  <div class="container-fluid p-0">
    <div class="row ">
      <div class="col-12 col-lg-6 col-md-6 col-sm-12 d-flex justify-content-start justify-content-md-end">
        <div class="col-left">
            <h2><?php the_field('jatc_training_title'); ?></h2>
            <?php the_field('jatc_training_content'); ?>
        </div>
      </div>
      <div class="col-12 col-sm-12 col-md-6 col-lg-6">
        <div class="col-right">
          <span>
           <?php the_field('jatc_training_right_content'); ?>
          </span>
        </div>
      </div>
    </div>
  </div>

</div>
<!-- best of best btf2 -->
<div class="being-apprentice">
  <div class="container-fluid p-0">
    <div class="row g-0">
      <div class="col-12 col-md-5 col-sm-12 d-flex justify-content-md-end justify-content-start">
       <div class="col-left">
         <h3>
          <?php the_field('being_apprentice_title'); ?>
         </h3>
         <p class="m-0">
           <?php the_field('being_apprentice_content'); ?>  
         </p>
       </div>
      </div>
      <div class="col-12 col-sm-12 col-md-7 d-flex">
        <div class="col-right">
          <iframe src="<?php the_field('being_apprentice_right_video'); ?>" width="100%" height="465" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="apprentice-requirment">
  <div class="container">
      <h4 class="text-white"><?php the_field('apprentice_requirment_title'); ?></h4>
    <div class="row g-0">
      <div class="col-12 col-sm-12 col-md-6">
        <div class="col-left">  
           <?php if (have_rows('apprentice_requirement_left_list')) { ?>
          <ul>
            <?php while (have_rows('apprentice_requirement_left_list')) { the_row(); ?>
            <li><?php the_sub_field('label'); ?></li>
            <?php } ?>
          </ul>
              <?php } ?>
        </div>
      </div>
      <div class="col-12 col-sm-12 col-md-6">
        <div class="col-right">
            <p class="text-white">
              <?php the_field('apprentice_requirement_right_content'); ?>
            </p>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- become a member sub page -->
<!-- facts -->
<div class="apprentice-facts">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-11 col-lg-9 col-xl-7  mx-auto">
        <div class="content text-center">
          <h6 class=""><?php the_field('apprentice_facts_title'); ?></h6>
          <p class="m-0">
            <?php the_field('apprentice_facts_content'); ?>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- facts -->

<!--hear from apprentice-->
<div class="hear-apprentice">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-11 col-lg-10 mx-auto p-0">
        <h2 class="text-center"><?php the_field('hear_apprentice_title'); ?></h2>
        <div class="video">
          <iframe src="<?php the_field('hear_apprentice_video'); ?>" width="100%" height="533" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </div>
</div>
<!--hear from apprentice- end->

<!-- journeyman job calls  sub page-->
<section class="journeyman-job-call-sp">
  <!-- journeyman-job-calls-banner -->
  <div class="journeyman-job-call-banner"> 
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12">
          <div class="banner-content">
            <div class="logo">
              <a href="<?php echo home_url(); ?>">
               <?php $header_logo = get_field('upload_website_logo', 'option'); ?>
              <?php if ( $header_logo ) : ?>
              <img src="<?php echo $header_logo['url']; ?>" alt="<?php echo $header_logo['alt']; ?>" />
              <?php endif; ?>
          </a>
            </div>
            <div class="title">
              <h1><?php echo get_the_title(); ?></h1>
            </div>
          </div>
        </div>        
      </div>
    </div>
  </div>
   <!-- journeyman-job-calls-banner -->  
  <!-- journeyman-job-calls-information btf1 -->
  <div class="journeyman-job-call-information">
    <div class="container">
      <div class="row g-0">
        <div class="col-12 col-sm-12 col-md-10 col-lg-8 col-xl-6">
          <div class="main">
            <h2><?php the_field('journeyman_job_calls_information_title'); ?></h2>
            <?php the_field('journeyman_job_calls_information_content'); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- journeyman-job-calls-information btf1 end-->

  <!-- journeyman-job-calls accordion -->
  <div class="journeyman-job-calls-accordion">
    <div class="container">
      <div class="row g-0">
        <div class="col-12 col-sm-12 col-lg-10">
           <?php if( have_rows('journeyman_jobs_calls_accordion') ):
              $i = 1; // Set the increment variable?>
          <div class="accordion journeyman-call-accordion" id="accordionExample">
            <?php  while ( have_rows('journeyman_jobs_calls_accordion') ) : the_row();
    
              $header = get_sub_field('accordion_header');
              $content = get_sub_field('accordion_content');
            ?>
            <div class="accordion-item">
              <h2 class="accordion-header" id="heading<?php echo $i;?>">
                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse<?php echo $i;?>" aria-expanded="true" aria-controls="collapse<?php echo $i;?>">
                   <?php echo $header; ?>
                </button>
              </h2>
              <div id="collapse<?php echo $i;?>" class="accordion-collapse collapse <?php if( $i == 1 ) { ?>show<?php } ?>" aria-labelledby="heading<?php echo $i;?>" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                  <div class="col-12 col-sm-12 col-md-10 col-lg-8">
                     <?php echo $content; ?>
                    </div>
                </div>
              </div>
            </div>
            <?php $i++; // Increment the increment variable
            endwhile; //End the loop ?>
          <!--  -->
          </div>
          <?php endif;?>
        </div>
      </div>
    </div>
  </div>
  <!-- journeyman-job-calls accordion end -->
   
</section>
<!-- journeyman job calls  sub page-->

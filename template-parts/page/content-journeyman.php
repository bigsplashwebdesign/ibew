<!-- banner start -->
<section class="journeyman-banner sp-banner">
  <div class="container"> 
    <div class="row">
      <div class="col-12 col-sm-12">
        <div class="banner-content">
          <div class="logo">
            <a href="<?php echo home_url(); ?>">
               <?php $header_logo = get_field('upload_website_logo', 'option'); ?>
              <?php if ( $header_logo ) : ?>
              <img src="<?php echo $header_logo['url']; ?>" alt="<?php echo $header_logo['alt']; ?>" />
              <?php endif; ?>
          </a>
          </div>
          <div class="title">
            <h1><?php echo get_the_title(); ?></h1>
          </div>
        </div>
      </div>        
    </div>
  </div>
</section>
<!-- banner end -->

<!-- journeyman-industry -->
<section class="journeyman-industry">
  <div class="container">
    <div class="row g-0">
      <div class="col-12 col-sm-12 col-md-10 col-lg-8 col-xl-6" >
        <div class="col-left">
          <h2><?php the_field('journeyman_title'); ?></h2>
          <?php the_field('journeyman_content'); ?>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- journeyman-industry-end -->
<!-- being-journeyman -->
<section class="being-journeyman">
  <div class="container">
    <h2>
      <?php the_field('being_journeyman_title'); ?>
    </h2>
    <div class="row g-0">
      <div class="col-12 col-sm-12 col-md-6">
        <div class="col-left">
          <p>
            <?php the_field('being_journeyman_content'); ?>        
          </p>
        </div>
      </div>
      <div class="col-12 col-sm-12 col-md-6">
        <div class="col-right">
          <p>
            <?php the_field('being_journeyman_right_content'); ?>
            
          </p>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- being-journeyman end-->

<!-- being ibew -->
<div class="being-ibew">
  <h3 class="text-center"><?php the_field('being_ibew_main_title'); ?></h3>
  <div class="container">
    <!-- p-0 p-sm-2 p-md-0 -->
    <div class="row">
      <div class="col-12">
        <div class="being-ibew-content">
          <?php if( have_rows('being_ibew_details') ) { 
                    $i = 0;
                ?>
          <div class="owl-carousel owl-theme">
             <?php while( have_rows('being_ibew_details') ) : the_row() ; ?>
            <div class="item">
              <?php $video_icon_jr = get_sub_field('video_icon');?>
              <?php $model_img_jr =  get_sub_field('model_image');?>
              <?php $video_url = get_sub_field('video_url');
                $img = 'https://vumbnail.com/'.$video_url.'.jpg';
              ?>
              <img src="<?php echo $video_icon_jr['url']; ?>" alt="<?php echo $video_icon_jr['alt']; ?>">
             <img onclick="openModal();currentSlide(<?php echo $i; ?>)" src="<?php echo $img; ?>">
            </div>
              <?php $i++; ?>
                  <?php endwhile; ?>
            
          </div>
           <?php  } ?>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- being ibew end -->

<!-- modal -->
<?php if( have_rows('being_ibew_details') ) { ?>
<div id="myModal" class="modal">
  <span class="close cursor" onclick="closeModal()">&times;</span>
  <div class="modal-content">
    <?php $em = 1; while( have_rows('being_ibew_details') ) : the_row() ; ?>
      <?php $video_url = get_sub_field('video_url');?>
      <div class="mySlides">
        <iframe id="iframe_<?php echo $em; ?>" class="cs-iframe" src="https://player.vimeo.com/video/<?php echo $video_url?>?h=3ea37d0df3&title=0&byline=0&portrait=0" width="100%" height="600" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
      </div>
    <?php $em++;endwhile; ?>
    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>
  </div>
</div>
 <?php  } ?>
<!-- modal -->

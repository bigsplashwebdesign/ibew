<?php get_header(); ?>

<div id="primary" class="site-content">
	<div id="content" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

		<?php switch (get_the_ID()):
				case 259:
				get_template_part('template-parts/page/content', 'member');
				break;
				case 262:
				get_template_part('template-parts/page/content', 'apprentice');
				break;
				case 265:
				get_template_part('template-parts/page/content', 'jobcalls');
				break;
				case 268:
				get_template_part('template-parts/page/content', 'journeymanjob');
				break;
				case 531:
				get_template_part('template-parts/page/content', 'inmemory');
				break;
				case 271:
				get_template_part('template-parts/page/content', 'paydues');
				break;
				case 274:
				get_template_part('template-parts/page/content', 'teledata');
				break;
				case 277:
				get_template_part('template-parts/page/content', 'cwce');
				break;
				case 332:
				get_template_part('template-parts/page/content', 'journeyman');
				break;
				case 390:
				get_template_part('template-parts/page/content', 'contact');
				break;
				default:
				get_template_part('template-parts/page/content', 'page');
			endswitch; ?>

		<?php endwhile; // end of the loop. ?>

	</div><!-- #content -->
</div><!-- #primary .site-content -->

<?php get_footer(); ?>
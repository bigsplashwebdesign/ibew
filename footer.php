<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package bigsplash
 * @since bigsplash 1.0
 */
?>
	<!-- footer start -->
<footer>
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12">
        <div class="footer-logo d-flex align-items-center justify-content-center">

        <a href="<?php echo home_url(); ?>">
        <?php $ftr_logo = get_field('footer_logo', 'option'); ?>
          <?php if ( $ftr_logo ) : ?>
          <img src="<?php echo $ftr_logo['url']; ?>" alt="<?php echo $ftr_logo['alt']; ?>" />
          <?php endif; ?>
          </a>

          <span><a href="<?php echo home_url(); ?>"> <?php the_field('footer_title', 'option'); ?></a></span>
        </div>
      </div>
      <div class="col-lg-3 col-md-4 col-sm-6 footer-list">
        <h5 class="footer-list-heading"><?php the_field('footer_menu_one_title', 'option'); ?></h5>
         <?php if( have_rows('footer_menu_one_detail', 'option') ): ?>
        <ul class="list-unstyled">
         <?php while( have_rows('footer_menu_one_detail', 'option') ): the_row(); ?>
           <li><a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('title'); ?></a></li>
           <?php endwhile; ?>
        </ul>
         <?php endif; ?>
      </div>
      <div class="col-lg-3 col-md-4 col-sm-6 footer-list">
        <h5 class="footer-list-heading"><?php the_field('footer_menu_two_title', 'option'); ?></h5>
       <?php if( have_rows('footer_menu_two_detail', 'option') ): ?>
        <ul class="list-unstyled">
          <?php while( have_rows('footer_menu_two_detail', 'option') ): the_row(); ?>
           <li><a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('title'); ?></a></li>
           <?php endwhile; ?>
        </ul>
        <?php endif; ?>
      </div>
      <div class="col-lg-3 col-md-4 col-sm-6 mt-0 mt-sm-5 mt-md-0 footer-list">
        <h5 class="footer-list-heading"><?php the_field('footer_menu_three_title', 'option'); ?></h5>
        <?php if( have_rows('footer_menu_three_detail', 'option') ): ?>
        <ul class="list-unstyled">
         <?php while( have_rows('footer_menu_three_detail', 'option') ): the_row(); ?>
           <li><a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('title'); ?></a></li>
           <?php endwhile; ?>
        </ul>
         <?php endif; ?>
      </div>
<!-- footer-contact us -->
      <div class="col-lg-3 col-md-6 col-sm-6 footer-list footer-contact-us mt-0 mt-sm-5 mt-lg-0">
        <div class="contact-us-detail">
          <h5>Contact</h5>
          <ul class="list-unstyled">
            <li class="location">
              <span><?php the_field('location_title', 'option'); ?></span><a target="" href="<?php the_field('address_link', 'option'); ?>"> <?php the_field('address', 'option'); ?></a>
            </li>
            <li class="">
              <span><?php the_field('phone_title', 'option'); ?></span><a href="tel:<?php the_field('header_phone_number', 'option'); ?>"><?php the_field('header_phone_number', 'option'); ?></a>
            </li>
            <li class="">
              <span><?php the_field('fax_title', 'option'); ?></span><a><?php the_field('fax_number', 'option'); ?></a>
            </li>
            <li class="">
              <span><?php the_field('job_line', 'option'); ?></span><a href="tel:<?php the_field('job_line_number', 'option'); ?> "><?php the_field('job_line_number', 'option'); ?></a>
            </li>
            <li class="">
              <span><?php the_field('toll_free_title', 'option'); ?></span><a href="tel:<?php the_field('toll_free_number', 'option'); ?>"><?php the_field('toll_free_number', 'option'); ?></a>
            </li>
          </ul>
        </div>
      </div>
<!--footer-contact us -->

  
    </div>
  </div>
</footer>
      <!-- Copyright -->
      <div class="copyright text-center">
        <?php the_field('footer_copyright', 'option'); ?>
      </div>
      <!-- Copyright -->
<!-- footer end -->
<script>
  function openModal() {
    document.getElementById("myModal").style.display = "block";
  }
  
  function closeModal() {
   
    var iframe = jQuery('.mySlides.active iframe')[0];
    var player = $f(iframe);
    player.api('pause');
    
     document.getElementById("myModal").style.display = "none";    
  }
  
  var slideIndex = 1;
  showSlides(slideIndex);
  
  function plusSlides(n) {
    showSlides(slideIndex += n);
  }
  
  function currentSlide(n) {
    showSlides(slideIndex = n);
  }
  
  function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("demo");
    var captionText = document.getElementById("caption");
    if (n > slides.length) {slideIndex = 1}
    if (n < 1) {slideIndex = slides.length}
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
        jQuery(slides[1]).removeClass('active');
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex-1].style.display = "block";
    
    jQuery('.mySlides').removeClass('active');
    jQuery(slides[slideIndex-1]).addClass('active');
    dots[slideIndex-1].className += " active";
    captionText.innerHTML = dots[slideIndex-1].alt;
  }
  </script>

<?php wp_footer(); ?>

</body>
</html>

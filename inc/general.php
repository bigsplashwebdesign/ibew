<?php
define( 'TEMPPATH', get_template_directory_uri());
define( 'IMAGES', TEMPPATH. "/assets/images");
$http_host = $_SERVER['HTTP_HOST'];
$localhost = strpos( $http_host, "localhost" );
$dev = strpos( $http_host, "dev." );
if ( $localhost !== false || $dev !== false ) {
	$dev = true;
} else {
	$dev = false;
}
define( 'LOCALHOST', $dev );


/*================================
	=Remove Notifications
==================================*/
add_action( 'after_setup_theme', 'bs_setup' );
function bs_setup() {
	add_theme_support( 'title-tag' );
}

/*======================
	=Excerpt
========================*/
function excerpt($limit) {
	$excerpt = explode(' ', get_the_excerpt(), $limit);
	if (count($excerpt)>=$limit) {
	array_pop($excerpt);
	$excerpt = implode(" ",$excerpt).'...';
	} else {
	$excerpt = implode(" ",$excerpt);
	}
	$excerpt = preg_replace('`[[^]]*]`','',$excerpt);
	$excerpt = strip_shortcodes($excerpt);
	return $excerpt;
}

add_action( 'after_setup_theme', 'bigsplash_setup' );
if ( ! function_exists( 'bigsplash_setup' ) ):
	function bigsplash_setup() {
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );

		if ( function_exists( 'add_image_size' ) ) {
			//add_image_size( 'mobile-slide', 320, 280, true ); //(cropped)
		}
		register_nav_menus( array(
			'primary' => 'Primary Menu'
		) );
	}
endif;

// Remove Short Links
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'start_post_rel_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'adjacent_posts_rel_link');

//Remove JQuery migrate
function isa_remove_jquery_migrate( &$scripts) {
    if(!is_admin()) {
        $scripts->remove( 'jquery');
        $scripts->add( 'jquery', false, array( 'jquery-core' ), '1.12.4' );
    }
}
add_filter( 'wp_default_scripts', 'isa_remove_jquery_migrate' );

function bs_deregister_dashicons()    {
	if (current_user_can( 'edit_others_pages' )) {
		 return;
	 }
	wp_deregister_style('dashicons');
}
add_action( 'wp_print_styles', 'bs_deregister_dashicons', 100 );

function remove_query_strings() {
   if(!is_admin()) {
       add_filter('script_loader_src', 'remove_query_strings_split', 15);
       add_filter('style_loader_src', 'remove_query_strings_split', 15);
   }
}

function remove_query_strings_split($src){
   $output = preg_split("/(&ver|\?ver)/", $src);
   return $output[0];
}
add_action('init', 'remove_query_strings');



//add_filter('show_admin_bar', '__return_false'); //remove admin bar

//ACF 
add_action('acf/init', 'my_acf_op_init');
function my_acf_op_init() {

    // Check function exists.
    if( function_exists('acf_add_options_page') ) {

        // Register options page.
        $option_page = acf_add_options_page(array(
            'page_title'    => __('Theme General Settings'),
            'menu_title'    => __('Theme Settings'),
            'menu_slug'     => 'theme-general-settings',
            'capability'    => 'edit_posts',
            'redirect'      => false
        ));
    }
}



// Allow SVG
add_filter('wp_check_filetype_and_ext', function ($data, $file, $filename, $mimes) {

	if (!$data['type']) {
		$wp_filetype = wp_check_filetype($filename, $mimes);
		$ext = $wp_filetype['ext'];
		$type = $wp_filetype['type'];
		$proper_filename = $filename;
		if ($type && 0 === strpos($type, 'image/') && $ext !== 'svg') {
			$ext = $type = false;
		}
		$data['ext'] = $ext;
		$data['type'] = $type;
		$data['proper_filename'] = $proper_filename;
	}
	return $data;
  
  
  }, 10, 4);
  

  add_filter('upload_mimes', function ($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
  });
  
  
  add_action('admin_head', function () {
	echo '<style type="text/css">
		 .media-icon img[src$=".svg"], img[src$=".svg"].attachment-post-thumbnail {
	  width: 100% !important;
	  height: auto !important;
	}</style>';
  });
  


add_action( 'admin_init', 'hide_editor' );
function hide_editor() {
  $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
  if( !isset( $post_id ) ) return;
  $homepage = get_the_title($post_id);
  $member = get_the_title($post_id);
  $apprentice = get_the_title($post_id);
  $contactpage = get_the_title($post_id);
  $cwcepage = get_the_title($post_id);
  $cwcejobs = get_the_title($post_id);
  $journeyman = get_the_title($post_id);
  $Journeymanjobs = get_the_title($post_id);
  $paydues = get_the_title($post_id);
  $teledata = get_the_title($post_id);
  if($homepage == 'Home' || $member == 'Become a Member' || $apprentice == 'Become an Apprentice' || $contactpage == 'Contact' || $cwcepage == 'CW CE' || $cwcejobs == 'CW CE Job Calls' || $journeyman == 'Journeyman' || $Journeymanjobs == 'Journeyman job calls' || $paydues == 'Pay Dues Online job calls' || $teledata == 'Tele Data Job Calls'){ 
    remove_post_type_support('page', 'editor');
  }
}

function ibew_login_logo() { 
?> 
<style type="text/css"> 
body.login div#login h1 a {
 	background-image: url(http://ibewdev.bigsplashmarketing.com/wp-content/uploads/ibewLogo250.png);  
	height:201px;
	width: 200px;
	background-size: 200px 201px;
	background-repeat: no-repeat;
	padding-bottom: 30px; 
} 
</style>
 <?php 
} 
add_action( 'login_enqueue_scripts', 'ibew_login_logo' );

?>

<?php
/*======================
	=Scripts
========================*/

function ibew_scripts() {   
    // Adding scripts file in the footer
   
    wp_enqueue_script('jquery-2.2.0.min-js', get_template_directory_uri() . '/assets/js/jquery-2.2.0.min.js', array( 'jquery' ), '', true);
    wp_enqueue_script('owl-carousel-min-js', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array( 'jquery' ), '', true);
    wp_enqueue_script('responsivemultimenu-js', get_template_directory_uri() . '/assets/js/responsivemultimenu.js', array( 'jquery' ), '', true);
    wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array( 'jquery' ), '', true);
	wp_enqueue_script('custom-js', get_template_directory_uri() . '/assets/js/custom.js', array( 'jquery' ), '', true);

    // Register main stylesheet
	wp_enqueue_style('owl-carousel-min-css', get_template_directory_uri() . '/assets/css/owl.carousel.min.css', array(), '', 'all');
    wp_enqueue_style('owl-theme-default-css', get_template_directory_uri() . '/assets/css/owl.theme.default.min.css', array(), '', 'all');
	wp_enqueue_style('bootstrap-css', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), '', 'all');
    wp_enqueue_style('gb-css', get_template_directory_uri() . '/assets/css/style.css', array(), '', 'all');
	
    // Register Open Sans Google Fonts
	wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css2?family=Work+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap', [], null );
}

add_action('wp_enqueue_scripts', 'ibew_scripts', 999);

?>

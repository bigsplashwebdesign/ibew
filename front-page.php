<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package bigsplash
 * @since bigsplash 1.0
 */

get_header(); ?>
<!-- banner start -->
<section class="banner">
  <div class="container">
    <div class="row g-0 d-flex align-items-center">
      <div class="col-12 col-sm-5 col-md-5 ">
        <div class="col-left d-flex justify-content-md-end justify-content-center">
        <a href="<?php echo home_url(); ?>">
         <?php $header_logo = get_field('upload_website_logo', 'option'); ?>
          <?php if ( $header_logo ) : ?>
          <img class="img-fluid" src="<?php echo $header_logo['url']; ?>" alt="<?php echo $header_logo['alt']; ?>" />
          <?php endif; ?>
          </a>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-6">
        <div class="col-right">
            <h1> <?php the_field('main_title'); ?></h1>
            <?php the_field('description'); ?>
        </div>
      </div>
    </div>
  </div>
  <video muted loop autoplay>
      <?php $file = get_field('uplode_video');
  if( $file ): ?>
    <source src="<?php echo $file['url']; ?>" >
<?php endif; ?>
  </video>
</section>
<!-- banner end -->

<!-- services btf1-->
    <section class="services">
        <div class="container-fluid p-0">
            <div class="row g-0">
              <?php if (have_rows('service_detail')) { ?>
                    <?php while (have_rows('service_detail')) { the_row(); ?>
                <div class="col-12 col-sm-12 col-md-4 mb-3 mb-md-0">
                    <div class="services-journeyman">
                    <?php $service_photo = get_sub_field('service_image');?>
                  <img class="img-fluid" src="<?php echo $service_photo['url']; ?>" alt="<?php echo $service_photo['alt']; ?>" />    
                        <div class="services-overlay d-flex justify-content-center align-items-center text-center flex-column">
                            <span><?php the_sub_field('service_title'); ?></span>
                            <?php the_sub_field('service_description'); ?>
                            <a class="sm-btn mb-2" href="<?php the_sub_field('service_link'); ?>"><?php the_sub_field('read_more'); ?></a>
                            <a class="sm-btn" href="<?php the_sub_field('job_calls_link'); ?>"><?php the_sub_field('job_calls_text'); ?></a>
                        </div>
                    </div> 
                </div>
                <?php } ?>
                  <?php } ?>
            </div>
        </div>
    </section>
<!-- services btf1-end -->

<!-- ibew-members start -->
<section class="ibew-members-bg">
  <div class="ibew-members">
      <!-- become apprentice -->
        <div class="become-member">
          <div class="container-fluid p-0">
            <div class="row g-0">
              <div class="col-11 col-sm-12 col-md-6 pe-0 pe-md-2 mb-3 mb-md-0 mx-auto">
                <div class="become-member-content d-flex align-items-center justify-content-center text-center flex-column">
                  <h2>
                    <?php the_field('become_a_member'); ?>
                  </h2>
                  <p>
                   <?php the_field('become_a_member_description'); ?>
                  </p>
                  <a href="<?php the_field('join_today_link'); ?>" class="sm-btn"><?php the_field('join_today'); ?></a>
                </div>
              </div>
              <div class="col-11 col-sm-12 col-md-6 ps-0 ps-md-2 mx-auto">
                <div class="become-apprentice d-flex align-items-center justify-content-center text-center flex-column">
                  <h2>
                    <?php the_field('become_an_apprentice'); ?>
                  </h2>
                  <p>
                    <?php the_field('become_a_apprentice_description'); ?>
                  </p>
                  <a href="<?php the_field('join_today_apprentice_link'); ?>" class="sm-btn"><?php the_field('join_today_apprentice'); ?></a>
                </div>
              </div>
            </div>
          </div>
        </div>
    <!-- become apprentice end -->

  

     <!-- being ibew -->
    <div class="being-ibew">
      <h3 class="text-center"><?php the_field('being_ibew_title'); ?></h3>
      <div class="container">
        <!-- p-0 p-sm-2 p-md-0 -->
        <div class="row">
          <div class="col-12">
            <div class="being-ibew-content">
              <?php if( have_rows('being_ibew_detail') ) { 
                    $num = 1;
                ?>
              <div class="owl-carousel owl-theme">
                    <?php while( have_rows('being_ibew_detail') ) : the_row() ; ?>
                <div class="item">
                   <?php $video_icon = get_sub_field('video_icon');?>
                   <?php $model_img =  get_sub_field('model_image');?>
                    <?php $video_url = get_sub_field('video_url');
                      $img = 'https://vumbnail.com/'.$video_url.'.jpg'
                    ?>
          
                  <img src="<?php echo $video_icon['url']; ?>" alt="<?php echo $video_icon['alt']; ?>">
                  <img onclick="openModal();currentSlide(<?php echo $num; ?>)" src="<?php echo $img; ?>">
                </div>
                       <?php $num++; ?>
                  <?php endwhile; ?>
               
              </div>
              <?php  } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- being ibew end -->

                  
    <!-- modal -->
    <?php if( have_rows('being_ibew_detail') ) { ?>
    <div id="myModal" class="modal">
      <span class="close cursor" onclick="closeModal()">&times;</span>
      <div class="modal-content">
        <?php $em = 1; while( have_rows('being_ibew_detail') ) : the_row() ; ?>
          <?php $video_url = get_sub_field('video_url');?>
          <div class="mySlides">
            <iframe id="iframe_<?php echo $em; ?>" class="cs-iframe" src="https://player.vimeo.com/video/<?php echo $video_url?>?h=3ea37d0df3&title=0&byline=0&portrait=0" width="100%" height="600" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
          </div>
       <?php $em++;endwhile; ?>
       <!--  <div class="mySlides">
          <iframe class="cs-iframe" src="https://player.vimeo.com/video/112820857?h=3ea37d0df3&title=0&byline=0&portrait=0" width="100%" height="600" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
        </div>
    
        <div class="mySlides">
          <iframe class="cs-iframe" src="https://player.vimeo.com/video/112821849?h=29703dd3fe&title=0&byline=0&portrait=0" width="100%" height="600" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
        </div>
        
        <div class="mySlides">
          <iframe class="cs-iframe" src="https://player.vimeo.com/video/112665247?h=79e4d47cc2&title=0&byline=0&portrait=0" width="100%" height="600" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
        </div> -->
        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next" onclick="plusSlides(1)">&#10095;</a>
      </div>
    </div>
    <?php  } ?>
    <!-- modal -->


    <!-- current members -->
    <div class="current-members">
      <div class="container-fluid p-md-0">
        <div class="row d-flex justify-content-between g-0">
          <div class="col-12 col-lg-6 col-md-5 col-sm-12 d-flex justify-content-start justify-content-md-end">
            <div class="col-left">
              <h2><?php the_field('current_member_title'); ?></h2>
              <p><?php the_field('current_member_description'); ?></p>
            </div>
          </div>
          <div class="col-12 col-sm-12 col-md-7 col-lg-6 ps-0 ps-md-3">
            <div class="col-right">
              <?php if (have_rows('current_member_detail')) { ?>
                    <?php while (have_rows('current_member_detail')) { the_row(); ?>
              <a href="<?php the_sub_field('member_link'); ?>" class="member-login-ibew">
                <?php $member_photo = get_sub_field('member_icon');?>
                  <img src="<?php echo $member_photo['url']; ?>" alt="<?php echo $member_photo['alt']; ?>" />  
                <div class="current-member-login d-flex justify-content-between align-items-center">
                  <div class="current-member-detail">
                    <h4><?php the_sub_field('member_title'); ?></h4>
                    <p><?php the_sub_field('member_description'); ?></p>
                  </div>
                  <img src="<?php bloginfo('template_url'); ?>/assets/images/ibew-arrow-right.png" class="ibew-hov-arrow" alt="">
                </div>
              </a>
               <?php } ?>
               <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- current member end -->
  </div>
</section>
<!-- ibew-members end -->   

<!-- contracting -->
<section class="contracting">
  <div class="container-fluid p-0">
    <div class="row g-0 align-items-center align-items-xxl-start">
      <div class="col-12 col-md-12 col-lg-5">
        <div class="col-left">
          <h2><?php the_field('contracting_title'); ?></h2>
          <?php the_field('contracting_description'); ?>
          <ul>
            <?php if (have_rows('contracting_list')) { ?>
                    <?php while (have_rows('contracting_list')) { the_row(); ?>
            <li><a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('title'); ?></a></li>
             <?php } ?> <?php } ?>
          </ul>
          <a href="#" class="sm-btn">
            Learn More
          </a>
        </div>
      </div>
      <div class="col-12 col-sm-12 col-lg-7 mx-auto">
        <div class="col-right">
          <div class="title"><?php the_field('skilled_labor_description'); ?></div>
          <div id="carouselExampleFade" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
              <?php if (have_rows('contracting_right_section')) { 
                $z = 0; ?>
                    <?php while (have_rows('contracting_right_section')) { the_row(); ?>
              <div class="carousel-item <?php if ($z==0) { echo 'active';} ?>" >
                <?php $cont_photo = get_sub_field('contracting_image');?>
                  <img src="<?php echo $cont_photo['url']; ?>" alt="<?php echo $cont_photo['alt']; ?>" />
              </div>
               <?php $z++; } ?> <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- contact us -->
<section class="contact-us">
  <div class="container">
    <div class="row g-0 d-flex justify-content-between">
      <div class="col-12 col-sm-12 col-md-5">
        <div class="col-left">
          <h2><?php the_field('contact_title', 'option'); ?></h2>
          <p><?php the_field('contact_description', 'option'); ?></p>
          <div class="contact-us-detail">
            <ul>
              <li class="location">
                <span><?php the_field('location_title', 'option'); ?></span><a target="" href="<?php the_field('address_link', 'option'); ?>"> <?php the_field('address', 'option'); ?></a>
              </li>
              <li class="">
                <span><?php the_field('phone_title', 'option'); ?></span><a href="tel:713-869-8900"><?php the_field('header_phone_number', 'option'); ?></a>
              </li>
              <li class="">
                <span><?php the_field('fax_title', 'option'); ?></span><a href="tel:713-868-6342"><?php the_field('fax_number', 'option'); ?></a>
              </li>
              <li class="">
                <span><?php the_field('job_line', 'option'); ?></span><a href="tel:713-869-1311 "><?php the_field('job_line_number', 'option'); ?></a>
              </li>
              <li class="">
                <span><?php the_field('toll_free_title', 'option'); ?></span><a href="tel:888-716-JOBS"><?php the_field('toll_free_number', 'option'); ?></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-12 col-md-6 mt-5 mt-md-0">
        <div class="col-right">
<!-- <form class="row g-3">
          <div class="col-md-6">
            <label for="inputEmail4" class="form-label ">First Name</label>
            <input type="text" class="form-control" id="inputEmail4" placeholder="First Name">
          </div>
          <div class="col-md-6">
            <label for="inputEmail4" class="form-label">Last Name</label>
            <input type="text" class="form-control" id="inputEmail4" placeholder="Last Name">
          </div>
          <div class="col-md-6">
            <label for="inputPassword4" class="form-label">Email</label>
            <input type="email" class="form-control" id="inputPassword4" placeholder="Email">
          </div>
          <div class="col-md-6">
            <label for="inputAddress" class="form-label">Phone number</label>
            <input type="number" class="form-control" id="inputAddress" placeholder="Number">
          </div>
          <div class="text-area-contactus d-flex flex-column">
          <label for="" class="form-label">Message</label>
          <textarea name="" id="" rows="3" placeholder="Additional information"></textarea>
          </div>
          <button class="sm-btn" type="submit">submit</button>
          </div>
        </form> -->
			<?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>
      </div>
      </div>
    </div>
  </div>
</section>
<!-- contact us end -->
<?php get_footer(); ?>
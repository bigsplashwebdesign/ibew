<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php wp_head(); ?>
<script src="https://player.vimeo.com/api/player.js"></script>
<script src="http://a.vimeocdn.com/js/froogaloop2.min.js"></script>
</head>

<!-- header start -->
<header>
  <div class="top-nav">
    <div class="container">
      <div class="row g-0 d-flex align-items-center">
        <div class="col-6">
          <div class="col-left ">
            <ul class="d-flex ">
                <li class="select-language">
                  ##########
                    <!-- <div id="google_translate_element"></div>
                    <script type="text/javascript">
                        function googleTranslateElementInit() {
                        new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'es', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
                        }
                        </script>
                        <script async type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script> -->
               
             </li> 
                <li>
                    <a href="<?php the_field('pay_dues_link', 'option'); ?>"><?php the_field('pay_dues', 'option'); ?></a>
                    
                </li>
                <li class="jobs-call">
                    <a href="<?php the_field('job_calls_link', 'option'); ?>"><?php the_field('job_calls', 'option'); ?></a>
                    <span class="arrow-down">
                        <img src="<?php bloginfo('template_url'); ?>/assets/images/arrow-down.svg" alt="">
                    </span>
                    <?php
                      $rows = get_field('job_calls_list', 'option');
                      if( $rows ): ?>
                        <div class="drop-down-menu">
                          <ul>
                            <?php 
                              foreach( $rows as $row ) { ?>
                                <li>
                                  <a href="<?php echo $row['job_calls_dropdown_page_link']; ?>"><?php echo $row['job_calls_dropdown_page_text']; ?></a>
                                </li>
                            <?php
                              }
                            ?>
                          </ul>
                        </div>
                    <?php endif; ?>
                </li>
                <li>
                    <a href="<?php the_field('news_link', 'option'); ?>"><?php the_field('news', 'option'); ?></a>
                </li>
                <li>
                    <span class="call-icon"><img src="<?php bloginfo('template_url'); ?>/assets/images/call-icon.svg" alt=""></span>
                    <a href="tel:<?php the_field('site_phone_number', 'option'); ?>"><?php the_field('site_phone_number', 'option'); ?></a>
                </li>
            </ul>
          </div>
        </div>
        <div class="col-6">
          <div class="col-right">
            <div class="member-login d-flex align-items-center justify-content-between">
              <span>Member Login</span>
              <div class="login-form">
                <form action="" class="member-form d-flex ">
                  <input type="text" class="top-nav-form me-0 me-md-1 me-xl-3" placeholder="Last Name">
                  <input type="number" class="top-nav-form" placeholder="Card #">
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="main-nav">
    <div class="container">
      <ul class="d-flex justify-content-between">
        <?php if( have_rows('header_menu', 'option') ): ?>
         <?php while( have_rows('header_menu', 'option') ): the_row(); ?>
        <li>
          <a href="<?php the_sub_field('main_menu_link'); ?>"><?php the_sub_field('main_menu'); ?></a>
        </li>
        <?php if( have_rows('sub_menu', 'option') ): ?>
         <?php while( have_rows('sub_menu', 'option') ): the_row(); ?>
        <li>
          <a href="<?php the_sub_field('sub_menu_link'); ?>"><?php the_sub_field('sub_menu_title'); ?>
            <span>
              <?php $sub_icon = get_sub_field('sub_menu_icon', 'option');?>
                  <img class="angle-down" src="<?php echo $sub_icon['url']; ?>" alt="<?php echo $sub_icon['alt']; ?>">
            </span></a>
            <div class="mega-menu">
              <div class="container <?php the_sub_field('container_class'); ?>">
                <ul class="<?php the_sub_field('list_class'); ?>">
                  <?php if( have_rows('sub_menu_dropdown', 'option') ): ?>
                       <?php while( have_rows('sub_menu_dropdown', 'option') ): the_row(); ?>
                  <li><a href="<?php the_sub_field('dropdown_link'); ?>"><?php the_sub_field('dropdown_title'); ?></a></li>
                  <?php endwhile; ?> <?php endif;?>
                  
                </ul>
              </div>
            </div>
        </li>
        <?php endwhile; ?> <?php endif;?>
        <?php endwhile; ?> <?php endif;?>
        <li>
          <span class="call-icon"><img src="<?php bloginfo('template_url'); ?>/assets/images/call-icon-color.svg" alt=""></span>
          <a href="tel:<?php the_field('site_phone_number', 'option'); ?>"><?php the_field('site_phone_number', 'option'); ?></a>
        </li>
      </ul>
      
    </div>
  </div>

      <!-- mobile-menu -->
      <?php the_field('mobile_menu', 'option'); ?>
 <!-- <--mobile-menu -->
</header>
<!-- header end -->
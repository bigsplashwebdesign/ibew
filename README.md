# Big Splash Gulp Theme

## To install:
1: Clone the repository into a fresh copy of Wordpress. (example: ```git clone git@bitbucket.org:bigsplashwebdesign/big-splash-gulp.git bigsplash```)

2: ``` cd bigsplash ```

3: Open **wpgulp.config.js** and edit line number 13 ```projectURL: '<YOUR WORDPRESS URL HERE>',``` (example: ```http://localhost:8888/sample-project```)

4: ``` npm install ```

5: ``` npm start ```

6: **Make sure the terminal window stays open**


## To Develop
### To Start Developing
1: cd into the **theme** folder

2: ``` npm start ```

### File Structure
**SCSS:** ``` assets/css/style.scss ``` & ``` assets/css/_variables.scss ```

**IMAGES:** ``` assets/images/raw/*.png|jpg|svg|gif ``` **(note: images put into the 'raw' folder will be compressed and copied to the assets/images folder)**

**JS:** ``` assets/js/custom/*.js ``` & ``` assets/js/vendor/*.js ``` **(note: upon save the js files will be concatenated into custom.min.js & vendor.min.js which the theme references)**
